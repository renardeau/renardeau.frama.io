#!/usr/bin/env python3
import os, re, sys, webbrowser
from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime
from hashlib import sha1
from pathlib import Path
from shutil import copyfile, copytree, rmtree

from bs4 import BeautifulSoup
from git import Repo
from jinja2 import Environment, FileSystemLoader
from livereload.server import Server
from PIL import Image

PORT = 8000
DIR = Path(__file__).parent
CSS_SRCS = ("font-awesome.min.css", "style.css")
JS_SRCS = ("jquery-3.7.1.min.js", "main.js")
OUT_DIR = Path(os.environ.get("OUT_DIR", DIR / "public"))
ROOT_URL = "https://renardeau.chezsoi.org"
DESCRIPTION = "Animation nature & contes"
HIDDEN_PAGES = ("fresques.html", "style-guide.html")
NOW = datetime.now().strftime("%Y-%m-%d")
SITEMAP_CHANGEFREQ = "daily"
STATS = defaultdict(int)

@dataclass
class Page:
    filename: str
    priority: float

def gen_out_dir():
    version = sum(1 for _ in Repo(DIR).iter_commits(all=True))
    for dir in ("files", "fonts", "img"):
        rmtree(str(OUT_DIR / dir), ignore_errors=True)
        copytree(str(DIR / dir), str(OUT_DIR / dir))
    for filename in ("favicon.ico", "icon.png"):
        copyfile(str(DIR / filename), str(OUT_DIR / filename))
    templates_dir = DIR / "templates"
    env = Environment(loader=FileSystemLoader(str(templates_dir)), autoescape=True,
                      trim_blocks=True, lstrip_blocks=True)
    pages = []
    for tmpl_file in templates_dir.glob("*.html"):
        template = env.get_template(tmpl_file.name)
        html_file = OUT_DIR / tmpl_file.name
        is_page_hidden = tmpl_file.name in HIDDEN_PAGES
        html = template.render(
            env=os.environ,
            root_url=ROOT_URL,
            description=DESCRIPTION,
            is_page_hidden=is_page_hidden,
            stats=STATS,
            version=version
        )
        html = insert_imgs_width_height(html)
        html_file.write_text(html)
        if not is_page_hidden:
            filename, priority = tmpl_file.name, 0.5
            if tmpl_file.name == "index.html":
                filename, priority = "", 1
            elif tmpl_file.name in ("mentions-legales.html", "ecoconception.html"):
                priority = 0.2
            pages.append(Page(filename, priority))
    # Rendering sitemap.xml :
    tmpl_file = templates_dir / "sitemap.xml"
    template = env.get_template(tmpl_file.name)
    (OUT_DIR / tmpl_file.name).write_text(template.render(
        root_url=ROOT_URL,
        pages=pages,
        changefreq=SITEMAP_CHANGEFREQ,
        now=NOW
    ))
    gen_css_bundle()
    gen_js_bundle()

def gen_css_bundle():
    short_hash = sha1(b"".join(cat(DIR / "css" / css_file) for css_file in CSS_SRCS)).hexdigest()[:7]
    css_bundle_filepath = f"bundles/bundle-{short_hash}.css"
    (OUT_DIR / "bundles").mkdir(exist_ok=True, parents=True)
    with (OUT_DIR / css_bundle_filepath).open("wb") as bundle_file:
        for css_filename in CSS_SRCS:
            bundle_file.write(cat(DIR / "css" / css_filename))
    sed("templates/partials/base.html", "bundles/bundle-[a-z0-9]+.css", css_bundle_filepath)

def gen_js_bundle():
    short_hash = sha1(b"".join(cat(DIR / "js" / js_file) for js_file in JS_SRCS)).hexdigest()[:7]
    js_bundle_filepath = f"bundles/bundle-{short_hash}.js"
    (OUT_DIR / "bundles").mkdir(exist_ok=True, parents=True)
    with (OUT_DIR / js_bundle_filepath).open("wb") as bundle_file:
        for js_filename in JS_SRCS:
            bundle_file.write(cat(DIR / "js" / js_filename))
    sed("templates/partials/base.html", "bundles/bundle-[a-z0-9]+.js", js_bundle_filepath)

def cat(filepath):
    with open(filepath, "rb") as f:
        return f.read()

def sed(filepath, pattern, value):
    with open(filepath, "r+") as f:
        data = f.read()
        if value in data: return
        f.seek(0)
        f.write(re.sub(pattern, value, data))
        f.truncate()

def insert_imgs_width_height(html):
    # Reco PageSpeed Insights : https://web.dev/articles/optimize-cls
    soup = BeautifulSoup(html, features="html.parser")
    for img in soup.find_all("img"):
        img_src = img.attrs["src"]
        if not img_src.endswith(".svg") and not img.attrs.get("width") and not img.attrs.get("height"):
            if img_src.startswith("/"):
                img_src = "." + img_src
            # print('Inserting "width" & "height" attributes to:', img)
            width, height = Image.open(img_src).size
            img.attrs["width"] = width
            img.attrs["height"] = height
    return soup.prettify(formatter="html5")

def compute_stats():
    css_size = list((OUT_DIR / "bundles").rglob("*.css"))[0].stat().st_size
    js_size = list((OUT_DIR / "bundles").rglob("*.js"))[0].stat().st_size
    fonts_total_size = 0
    for font_filepath in (OUT_DIR / "fonts").rglob("*.*"):
        fonts_total_size += font_filepath.stat().st_size
    html_total_size = 0
    for html_filepath in (OUT_DIR).rglob("*.html"):
        html_total_size += html_filepath.stat().st_size
    img_total_size = 0
    for img_filepath in (OUT_DIR / "img").rglob("*.*"):
        img_total_size += img_filepath.stat().st_size
    total_size = css_size + js_size + html_total_size + fonts_total_size + img_total_size
    return {
        "fonts_total_size_in_mib": fonts_total_size / 1024 / 1024,
        "css_size_in_mib": css_size / 1024 / 1024,
        "html_total_size_in_mib": html_total_size / 1024 / 1024,
        "images_total_size_in_mib": img_total_size / 1024 / 1024,
        "js_size_in_mib": js_size / 1024 / 1024,
        "total_size_in_mib": total_size / 1024 / 1024,
    }

rmtree(str(OUT_DIR / "bundles"), ignore_errors=True)
gen_out_dir()
STATS = compute_stats()
# 2nd build now that STATS is initialized:
gen_out_dir()

if "--watch" in sys.argv:
    SERVER = Server()
    for dir in ("css", "files", "fonts", "img", "js", "templates",):
        for filepath in (DIR / dir).rglob("*.*"):
            SERVER.watch(str(filepath), gen_out_dir)
    webbrowser.open(f"http://localhost:{PORT}")
    SERVER.serve(root=str(OUT_DIR), port=PORT)
