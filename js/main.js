jQuery.noConflict();
(function($) {
  $(window).on("load", function() {
    "use strict";
    var setWrapTopPadding = function() {
      if ($("body").is(".sticky-menu")) {
        var windowWidth = $(window).width(),
          headerHeight = $("header.header").outerHeight(),
          i = 3.5;
        if (windowWidth >= 960) {
          i = 2.5;
        }
        if (headerHeight > 68) {
          $(".wrap.full-wrap").css("padding-top", headerHeight / 16 + i + "em");
        }
      }
    };
    setWrapTopPadding();
    $(window).resize(setWrapTopPadding);
  });
  $(document).ready(function() {
    "use strict";
    $("html").addClass("js").removeClass("no-js");
    if ($("body").is(".home")) {
      var headerHeight = $("header.header").outerHeight();
      $("a.smooth-scroll").on("click", function() {
          if (location.pathname.replace(/^\//, "") === this.pathname.replace(/^\//, "") && location.hostname === this.hostname) {
          var targetElem = $(this.hash);
          if (targetElem = targetElem.length ? targetElem : $("[name=" + this.hash.slice(1) + "]"), targetElem.length) {
              $("html,body").animate({
              scrollTop: targetElem.offset().top - headerHeight
              }, 750);
              return false;
          }
          }
      });
      var setHeroHeight = function() {
        var windowHeight = $(window).height();
        if ($(".hero").length) {
          $(".hero").css("height", windowHeight).css("position", "relative");
        }
      };
      setHeroHeight();
      $(window).resize(setHeroHeight)
    }
    if ($("#btt").length > 0) { // back-to-top
      $("#btt").on("click", function(event) {
        event.preventDefault();
        $("html, body").animate({
          scrollTop: 0
        }, 800)
      });
    }
    $(".menu-trigger").on("click", function(event) {
      event.preventDefault();
      $("body").toggleClass("menu-active");
    });
    $(".mt-tabs").each(function() {
      var childDivs = $(" > div", this);
      $(" ul a", this).click(function(event) {
        event.preventDefault();
        childDivs.hide().filter(this.hash).show();
        $(this).parent().parent().find("li").removeClass("tab-active");
        $(this).parent().addClass("tab-active");
        return false;
      }).filter(":first").click()
    });
    $(".toggle").each(function() {
      var toggleInner = $(".toggle-inner", this).hide(),
          toggleTitle = $(".toggle-title", this);
      if ($(this).attr("data-id") === "open") {
        toggleInner.show();
        toggleTitle.addClass("active");
      }
      toggleTitle.on("click", function(event) {
        event.preventDefault();
        toggleInner.slideToggle();
        toggleTitle.toggleClass("active");
      })
    });
  });
}(jQuery));