https://renardeau.chezsoi.org

<!-- Next:
* https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/numecodiag/
* optimiser PDF ?
* référencement
  + à voir avec Laetitia :
    - fiche d'établissement Google pas visible dans résultats de recherche
    - Missing required address property for Local Business Google Search feature
  + https://search.google.com/search-console?resource_id=sc-domain%3Arenardeau.chezsoi.org
  + https://ahrefs.com/fr/webmaster-tools
  + https://www.bing.com/webmasters/about

Inspis:
* https://boisdeleppo.fr
* https://cpie-mayenne.org
-->

## Template HTML employé
* <https://freehtml5.co/itsy-free-full-featured-cutesy-little-html5-template/>
<!-- Autre envisagé : https://html5up.net/photon -->

## Charte graphique
Polices :
* Amatic SC
* Knewave
* Yeseva One

Couleurs de base :
* moonstone blue : `#6bb5b6`
* atomic tangerine : `#f69a6e`
* noir

Couleurs secondaires :
* seashell : `#fdf1ec`
* apricot : `#f7d1b9`
* cyan cornflower blue : `#2483a3`

## Lancement en local

    pip install -r requirements.txt
    ./gen.py --watch

Installation des hooks de pre-commit `git`:

    pre-commit install
    curl -ROLs https://github.com/validator/validator/releases/download/latest/vnu.linux.zip \
      && unzip vnu.linux.zip \
      && rm vnu.linux.zip

Exécution manuelle de tous les hooks :

    pre-commit run --all-files

Liste des classes CSS potentiellement inutiles :

    pre-commit run detect-missing-css-classes --all-files --verbose
